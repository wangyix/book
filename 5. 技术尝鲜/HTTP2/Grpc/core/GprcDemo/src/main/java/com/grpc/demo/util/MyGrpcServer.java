package com.grpc.demo.util;

import com.grpc.demo.grpc.GreetingServiceGrpc;
import com.grpc.demo.grpc.HelloRequest;
import com.grpc.demo.grpc.HelloResponse;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;

/**
 * @author 小测试
 * @create 2021-08-16 11:38
 */
public class MyGrpcServer {
    static public void main(String [] args) throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(11111)
                .addService(new GreetingServiceImpl()).build();

        System.out.println("Starting server...");
        server.start();
        System.out.println("Server started!");
        server.awaitTermination();
    }

    public static class GreetingServiceImpl extends GreetingServiceGrpc.GreetingServiceImplBase {
        @Override
        public void greeting(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
            System.out.println(request);

            String greeting = "你好啊, " + request.getName();

            HelloResponse response = HelloResponse.newBuilder().setGreeting(greeting).build();

            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }
    }
}
