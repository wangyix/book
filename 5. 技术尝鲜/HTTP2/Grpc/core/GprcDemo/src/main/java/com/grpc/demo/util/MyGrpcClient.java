package com.grpc.demo.util;

import com.grpc.demo.grpc.GreetingServiceGrpc;
import com.grpc.demo.grpc.HelloRequest;
import com.grpc.demo.grpc.HelloResponse;
import com.grpc.demo.grpc.Sentiment;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

/**
 * @author 小测试
 * @create 2021-08-16 14:11
 */
public class MyGrpcClient {
    public static void main(String[] args) throws InterruptedException {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext()
                .build();

        GreetingServiceGrpc.GreetingServiceBlockingStub stub =
                GreetingServiceGrpc.newBlockingStub(channel);

        HelloResponse helloResponse = stub.greeting(
                HelloRequest.newBuilder()
                        .setName("Ray")
                        .setAge(18)
                        .setSentiment(Sentiment.HAPPY)
                        .build());

        System.out.println(helloResponse);

        channel.shutdown();
    }
}
