> 参考文档：
>
> 运行原理：https://www.mikesay.com/2021/01/17/sonar-plugin-principle/
>
> CI工作流程：http://www.mydlq.club/article/11/
>
> 详情文档：[https://gitbook.curiouser.top/origin/SonarQube%E9%9D%99%E6%80%81%E4%BB%A3%E7%A0%81%E6%89%AB%E6%8F%8F%E5%88%86%E6%9E%90%E7%AE%80%E4%BB%8B.html](https://gitbook.curiouser.top/origin/SonarQube静态代码扫描分析简介.html)



# 1. SonarQube报告

- 首页

  <img src="https://imgfigure1.oss-cn-hangzhou.aliyuncs.com/20210618172925.png" style="zoom:50%;" />

- Python	

  <img src="https://imgfigure1.oss-cn-hangzhou.aliyuncs.com/20210618172441.png" style="zoom:50%;" />

- Java

  <img src="https://imgfigure1.oss-cn-hangzhou.aliyuncs.com/20210618172708.png" style="zoom:50%;" />

# 2. SonarQube CI流程

<img src="https://imgfigure1.oss-cn-hangzhou.aliyuncs.com/20210618182522.png" style="zoom:100%;" />

# 3. SonarQube安装

> GitHub地址：[SonarSource/sonarqube: Continuous Inspection (github.com)](https://github.com/SonarSource/sonarqube)

- 下载安装包，选择版本（建议7.6）

<img src="https://imgfigure1.oss-cn-hangzhou.aliyuncs.com/20210618182811.png" style="zoom:50%;" />

![image.png](https://cdn.nlark.com/yuque/0/2021/png/738847/1624271096185-2f685e78-c909-46d0-9e74-505ef0d3d11d.png#align=left&display=inline&height=151&margin=%5Bobject%20Object%5D&name=image.png&originHeight=151&originWidth=557&size=6255&status=done&style=none&width=557)

