定时任务这个功能，在日常工作中很经常用到，例如：

1. 接口自动化/UI自动化   每天的定时巡检
1. 数据库每天晚上的定时同步
1. 每天定时删除数据库中的脏数据/或者每天自动恢复测试数据



<a name="CZx46"></a>
# 1. 注解Scheduled
<a name="bhdbT"></a>
## 1.1 Scheduled源码
```
//官方提供的参数  单位都是毫秒
public @interface Scheduled {
    String CRON_DISABLED = "-";

    String cron() default "";//常用

    String zone() default "";

    long fixedDelay() default -1L;//常用

    String fixedDelayString() default "";

    long fixedRate() default -1L;//常用

    String fixedRateString() default "";

    long initialDelay() default -1L;

    String initialDelayString() default "";
}
```
<a name="byeue"></a>
## 1.2 fixedDelay和fixedRate的区别

   - 参考文章：[https://www.cnblogs.com/javahr/p/8318642.html](https://www.cnblogs.com/javahr/p/8318642.html)

![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623981946738-bfacb9a1-745f-4216-b1f4-38978b143585.png#align=left&display=inline&height=958&id=cqUAR&margin=%5Bobject%20Object%5D&originHeight=958&originWidth=2476&size=0&status=done&style=none&width=2476)
<a name="iIPxn"></a>
## 1.3 代码示例
<a name="42aFK"></a>
### 1.3.1 fixedDelay   

- 任务执行之后开始执行下次任务

fixedDelay为5S，Thread.sleep(2000)阻塞2S，相当于两次任务间隔7S
```
@Configuration  //1. 标记配置类
@EnableScheduling   //2. 开启定时任务
public class SchedulConfig {
    //3. 添加定时任务
    @Scheduled(fixedDelay = 5000)
    private void configUreTasks() throws InterruptedException {
        Thread.sleep(2000);
        log.error("从定时任务开始，任务就开始执行---->："+LocalDateTime.now());
    }
```
![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623982915487-c6c2936f-9539-4ee1-b632-6e9335c7b4a2.png#align=left&display=inline&height=1080&id=NF7w6&margin=%5Bobject%20Object%5D&originHeight=1080&originWidth=1712&size=0&status=done&style=none&width=1712)
<a name="uAouw"></a>
### 1.3.2 fixedRate  
> **fixedRate 每次任务结束后会从任务编排表中找下一次该执行的任务，判断是否到时机执行。fixedRate 的任务某次执行时间再长也不会造成两次任务实例同时执行，除非用了 @Async 注解。 fixedDelay 总是前一次任务完成后，延时固定长度然后执行一次任务**

- 任务执行时执行下次任务

fixedRate  任务间隔5S，Thread.sleep(2000)  任务阻塞2S，相当于任务间隔5S<br />

```
@Configuration  //1. 标记配置类
@EnableScheduling   //2. 开启定时任务
public class SchedulConfig {
    //3. 添加定时任务
    @Scheduled(fixedRate = 5000)
    private void configUreTasks() throws InterruptedException {
        Thread.sleep(2000);
        log.error("从定时任务开始，任务就开始执行---->："+LocalDateTime.now());
    }
 }
```
![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623982670913-5f6b1c8e-4021-4056-a55a-50617e4492a7.png#align=left&display=inline&height=910&id=K1ANw&margin=%5Bobject%20Object%5D&originHeight=910&originWidth=1704&size=0&status=done&style=none&width=1704)
<a name="zSdz3"></a>
### 1.3.3 cron


<a name="wfNjn"></a>
#### 1.3.3.1 概述
cron是计划任务（约定时间），例如：每分钟第10S的时候开始执行任务；
<a name="yjX72"></a>
#### 1.3.3.2 示例
cron = "30 * * * * ? "  表示  每分钟的第30秒之后开始执行 <br />Thread.sleep(2000)    阻塞2秒<br />表示每分钟的第32秒开始执行<br />

```
@Configuration  //1. 标记配置类
@EnableScheduling   //2. 开启定时任务
public class SchedulConfig {
    //3. 添加定时任务
    @Scheduled(cron = "30 * * * * ? ")
    private void configUreTasks() throws InterruptedException {
        Thread.sleep(2000);
        log.error("cron,每分钟的32秒输出（30秒开始执行，2秒阻塞）---->："+LocalDateTime.now());
    }

```

<br />![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623995046035-8ba6ec46-04ca-4f33-9d7c-1a45a7bbe500.png#align=left&display=inline&height=1101&id=XxW5D&margin=%5Bobject%20Object%5D&originHeight=1101&originWidth=1926&size=0&status=done&style=none&width=1926)
<a name="4NMYU"></a>
#### 1.3.3.2 cron表达式

- 手动编写

![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623994605146-edd9c682-0b25-4f03-98d6-f363d8cea972.png#align=left&display=inline&height=758&id=OexxV&margin=%5Bobject%20Object%5D&originHeight=758&originWidth=1702&size=0&status=done&style=none&width=1702)

- 在线生成

[https://cron.qqe2.com/](https://cron.qqe2.com/)<br />![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623995395876-873bd21d-7d3e-4aab-86a1-f6dc2ac50a0c.png#align=left&display=inline&height=1204&id=pTE8E&margin=%5Bobject%20Object%5D&originHeight=1204&originWidth=1604&size=0&status=done&style=none&width=1604)
<a name="GTvws"></a>
# 2. 接口_SchedulingConfigurer_
> 参考文档：[https://www.cnblogs.com/mmzs/p/10161936.html](https://www.cnblogs.com/mmzs/p/10161936.html)

<a name="bwQ7M"></a>
## 2.1 示例


- 表里的cron表达式    0 * * * * ?     


<br />![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623996330549-118ac920-bd50-4bc1-bb04-433a0db2f189.png#align=left&display=inline&height=1040&id=XbmAp&margin=%5Bobject%20Object%5D&originHeight=1040&originWidth=2094&size=0&status=done&style=none&width=2094)

- 执行的结果

![](https://cdn.nlark.com/yuque/0/2021/png/738847/1623996252476-638eb7e2-f54c-407d-876f-a162bfd304bf.png#align=left&display=inline&height=1207&id=hDekd&margin=%5Bobject%20Object%5D&originHeight=1207&originWidth=2192&size=0&status=done&style=none&width=2192)
<a name="apfyo"></a>
## 2.2 步骤
> 我用的是mybatis plus   



- 获取表里的数据
```
@Service
public class CronServiceImpl extends ServiceImpl<CronMapper, Cron> implements CronService {
    @Autowired
    CronMapper cronMapper;

    @Override
    public String getCron() {
        QueryWrapper<Cron> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("cron");
        Cron cron = cronMapper.selectOne(queryWrapper);
        String cronString = cron.getCron();
        return cronString;
    }

}
```

- 定时任务的代码
```
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
//@EnableScheduling   // 2.开启定时任务
public class DynamicScheduleTaskService implements SchedulingConfigurer {
    @Autowired
    private CronService cronService;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(
                //1.添加任务内容(Runnable)
                () -> System.out.println("执行定时任务2: " + LocalDateTime.now().toLocalTime()),
                //2.设置执行周期(Trigger)
                triggerContext -> {
                    //2.1 从数据库获取执行周期
                    String cron = cronService.getCron();//   通过接口获取表里的cron
                    log.debug("---》" + cron);
                    //2.2 合法性校验.
                    if (StringUtils.isEmpty(cron)) {
                        // Omitted Code ..
                    }
                    //2.3 返回执行周期(Date)
                    return new CronTrigger(cron).nextExecutionTime(triggerContext);
                });
    }
}

```


